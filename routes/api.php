<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::group(['prefix'=>'auth'], function(){
    
    Route::post('login', 'MainController@login');
    Route::post('register', 'MainController@register');
    Route::post('profile/register', 'MainController@profileUpdate');

        Route::group(['middleware' => 'auth:api'], function(){

            Route::post('logout', 'MainController@logout');
            Route::post('user/profile', 'MainController@profile');
            Route::post('exercise', 'MainController@exercise');
            Route::post('exercise/catagories', 'MainController@exercise_catagories');
            Route::post('exercise/{id}', 'MainController@exerciseid');
            Route::post('exercise/all/{id}', 'MainController@exerciseAllid');
            Route::post('exercise/search/{id}', 'MainController@exerciseid');
            Route::post('nutrition', 'MainController@nutrition');
            Route::post('nutrition/{id}', 'MainController@nutrition_id');
            Route::post('nutrition/all/{id}', 'MainController@nutritionAllid');
            Route::post('nutritioncat', 'MainController@nutrition_catagories');
            Route::post('blog', 'MainController@nutrition');
            Route::post('store', 'MainController@store');
            Route::post('store/catagories', 'MainController@store_catagories');
            Route::post('user', 'MainController@user');
            Route::post('get_weekly', 'MainController@getWeekly');
            Route::post('get_weekly/monday', 'MainController@monday');
            Route::post('get_weekly/tuesday', 'MainController@tuesday');
            Route::post('get_weekly/wednesday', 'MainController@wednesday');
            Route::post('get_weekly/thursday', 'MainController@thursday');
            Route::post('get_weekly/friday', 'MainController@friday');
            Route::post('get_weekly/saturday', 'MainController@saturday');
            Route::post('get_weekly/sunday', 'MainController@sunday');


    });

});
