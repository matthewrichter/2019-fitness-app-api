<?php
use App\Applicant;
use App\User;
use App\Week;
use App\Store;
use App\Storecat;
use App\Profile;
use App\Exercise;
use App\Exercisecat;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});


Route::get('test', function(){ return '';});

Route::resource('profile', 'ProfileController');
Route::resource('nutrition', 'NutritionController');
Route::resource('nutritioncat', 'NutritioncatController');
Route::resource('exercise', 'ExerciseController');
Route::resource('exercisecat', 'ExercisecatController');
Route::resource('storecat', 'StorecatController');
Route::resource('view-profile', 'ViewProfileController');
Route::resource('categories', 'CategoriesController');
Route::resource('blogs', 'BlogController');