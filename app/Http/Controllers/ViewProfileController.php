<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use App\User;
use DB;

class ViewProfileController extends Controller
{

    public function index()
    {
        return view('profiles.view');
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {

        $user = User::find($id);
        $profile = $user->profile;

        return view('profiles.view', compact('profile','user'));
    }


    public function edit($id)
    {
        return 'This is edit id '.$id;
    }


    public function update(Request $request, $id)
    {


        $update = User::findOrFail($id);
        $update->name = $request->name;
        $update->email = $request->email;
        $update->save();

        return redirect()->back()->with('updated','true');

    }


    public function destroy($id)
    {
        //
    }
}
