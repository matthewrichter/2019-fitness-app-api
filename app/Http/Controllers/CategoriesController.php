<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use App\User;
use App\Exercisecat;
use App\Nutritioncat;
use App\Storecat;


class CategoriesController extends Controller
{

    public function index()
    {
        $profiles = User::all();
        $catagories = Exercisecat::orderBy('id', 'asc')->get();
        $nutrition = Nutritioncat::orderBy('id', 'asc')->get();
        $stores = Storecat::orderBy('id', 'asc')->get();
        $lastid_n = Nutritioncat::orderBy('id', 'desc')->first();
        $last_n = $lastid_n['id'];
        $lastid_w = Exercisecat::orderBy('id', 'desc')->first();
        $last_w = $lastid_w['id'];
        $lastid_s = Storecat::orderBy('id', 'desc')->first();
        $last_s = $lastid_s['id'];
        return view('categories.index', compact('profiles','catagories', 'nutrition', 'stores','last_n','last_w','last_s'));
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
