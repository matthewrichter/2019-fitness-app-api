<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Storecat;

class StorecatController extends Controller
{

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
   {
        $request->validate([
        'catagorie_name' => 'required',
        ]);
        
        Storecat::create($request->all());
        
        return redirect()->back()
        ->with('added','true');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy(Storecat $storecat)
    {
       $storecat->delete();

       return redirect()->back()->with('deleted','true');
    }
}
