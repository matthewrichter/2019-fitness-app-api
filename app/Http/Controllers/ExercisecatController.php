<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exercisecat;
use Illuminate\Support\Facades\Storage;

class ExercisecatController extends Controller
{
 
    public function index()
    {
        //
    }


    public function create()
    {
       // 
    }


    public function store(Request $request)
    {
            $request->validate([
            'catagorie_name' => 'required',
            'img'     =>  'required|image|mimes:jpeg,png,jpg,gif|max:2048'
            ]);


        $storefile = Storage::disk('public') ->put('images',$request->file('img'));
        $name = basename($storefile);


        Exercisecat::create([
            'catagorie_id' => $request->catagorie_id,
            'catagorie_name' => $request->catagorie_name,
            'img' => $name
        ]);

        return redirect()->back()->with('added','true');
    }


    public function show($id)
    {
        return Exercisecat::all()->where('id',$id);
    }


    public function edit($id)
    {
       return 'this is edit';
    }

 
    public function update(Request $request, $id)
    {
        //
    }

    public function destroy(Exercisecat $exercisecat)
    {
        $exercisecat->delete();

        return redirect()->back()->with('deleted','true');
    }
}
