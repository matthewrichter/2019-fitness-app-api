<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nutritioncat;
use Illuminate\Support\Facades\Storage;

class NutritioncatController extends Controller
{

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $request->validate([
            'catagorie_name' => 'required',
            'img'     =>  'required|image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        $storefile = Storage::disk('public') ->put('images',$request->file('img'));
        $name = basename($storefile);


        Nutritioncat::create([
            'catagorie_id' => $request->catagorie_id,
            'catagorie_name' => $request->catagorie_name,
            'img' => $name
        ]);
        
        return redirect()->back()
        ->with('added','true');

    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy(Nutritioncat $nutritioncat)
    {

        $nutritioncat->delete();

        return redirect()->back()->with('deleted','true');
    }
}
