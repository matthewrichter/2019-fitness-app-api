<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use Illuminate\Support\Facades\Storage;

class BlogController extends Controller
{

    public function index()
    {
        $blogs = Blog::all();

        return view('blogs.index', compact('blogs'));
    }


    public function create()
    {
        //
    }


    public function store(Request $request, Blog $blog)
    {
        $request->validate([
            'title' => 'required',
            'img'     =>  'required|image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        $storefile = Storage::disk('public') ->put('images',$request->file('img'));
        $name = basename($storefile);

        $blog->create([
            'title' => $request->title,
            'short' => $request->short,
            'description' => $request->description,
            'img' => $name
        ]);

        return redirect()->back()
            ->with('addedb','true');
    }


    public function show($id)
    {
        return Blog::all()->where('id',$id);
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy(Blog $blog)
    {
        $blog->delete();

        return redirect()->back()->with('deletedb','true');
    }
}
