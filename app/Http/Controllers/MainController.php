<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Applicant;
use App\User;
use App\Exercise;
use App\Exercisecat;
use App\Bmi;
use App\Week;
use App\Nutrition;
use App\Nutritioncat;
use App\Blog;
use App\Store;
use App\Storecat;
use App\Profile;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class MainController extends Controller
{

    public function register(Request $request){

        // Validate incoming registration request
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed'
        ]);

        // If validation success, create new user
        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        // Save user to database and return json response
        $user->save();

        //Generate a random profile
        $profile = new Profile([
            'user_id' => $user->id,
            'dob' => '2000-01-01',
            'height' => 0,
            'weight' => 0,
            'age' => 0,
            'sex' => 'None',
            'ethnic' => 'None'
        ]);

        $profile->save();

//        return response()->json([
//            'message' => 'success',
//        ], 201);

        return $user;


    }

    public function login(Request $request){

        // Validate incoming login request
        $request->validate([
            'email' => 'required|string|email',
            'remember_me' => 'boolean'
        ]);

        //Validate username
        $email_validate = User::where('email',$request->email)->first();

        if(!$email_validate){
            return response()->json([
                'message' => 'Username incorrect',
                'code' => 401
            ],401);
        }


        // Make login attempt and return token data
        $credentials = request(['email','password']);

        if(!Auth::attempt($credentials))

            // if username passed validation and Auth::attempt fails, then password is incorrect
            return response()->json([
                'message' => 'Password incorrect',
                'code' => 401
            ],401);

            $user = $request->user();
            $result = $user->createToken('UMP Token');
            $token = $result->token;

            if($request->remember_me){
                $token->expires_at = Carbon::now()->addWeeks(2);
            }

            $token->save();

            return response()->json([
                'access_token' => $result->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse($result->token->expires_at)->toDateString(),
                'status' => 1
            ],200);


    }

    // Log user out
    public function logout(Request $request){

        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'User logged out',
        ]);

    }

    // Return  profile
    public function profile(Request $request){

        try {

//            $users = User::with('weeks.applicant.bmis')->where('id',$request->user()->id)->get();
//            return response()->json($users);

            $user = User::find($request->user()->id);
            return $user->profile;

        } catch (Exception $e) {

            return response()->json([
                'message' => 'No Access',
                'status' => 0,
                'code' => 401
            ],401);
        }


    }

    // Return  user
    public function user(Request $request){

        try {

            $user = User::find($request->user()->id);
            return $user;

        } catch (Exception $e) {

            return response()->json([
                'message' => 'No Access',
                'status' => 0,
                'code' => 401
            ],401);
        }


    }

    // Return exercise list
    public function exercise(Request $request){

        try {

            $exercise = Exercise::all();
            return $exercise;

        } catch (Exception $e) {

            return response()->json([
                'message' => 'No Access',
                'status' => 0,
                'code' => 401
            ],401);
        }


    }

    // Return exercise catagories
    public function exercise_catagories(Request $request){

        try {

            $exercisecat = Exercisecat::all();
            return $exercisecat;

        } catch (Exception $e) {

            return response()->json([

                'error' => 'no persmission',
                'message' => 'no catagories found',
                'status' => 0,
                'code' => 401

            ],401);

        }


    }
    // Return exercise all id
    public function exerciseAllid($id){

        try {

            $exercise = Exercise::all()->where('catagorie_id',$id);

            if($exercise->isEmpty()) {

                return response()->json([

                    'message' => 'no items found',
                    'status' => 0,
                    'code' => 401

                ]);

            }else{
                return response()->json($exercise);
            }

        } catch (Exception $e) {

            return response()->json([

                'error' => 'no persmission',
                'message' => 'no items found',
                'status' => 0,
                'code' => 401

            ],401);

        }


    }
    
        // Return exercise all id
    public function exerciseid($id){

        try {

            $exercise = Exercise::findOrFail($id);
           
                return response()->json($exercise);
            

        } catch (Exception $e) {

            return response()->json([

                'error' => 'no persmission',
                'message' => 'no items found',
                'status' => 0,
                'code' => 401

            ],401);

        }


    }

    // Return nutrition id
    public function nutritionid($id){

        try {

            $nutrition = Nutrition::all()->where('catagorie_id',$id);

            if($nutrition->isEmpty()) {

                return response()->json([

                    'message' => 'no items found',
                    'status' => 0,
                    'code' => 401

                ]);

            }else{
                return response()->json($nutrition);
            }




        } catch (Exception $e) {

            return response()->json([

                'error' => 'no persmission',
                'message' => 'no items found',
                'status' => 0,
                'code' => 401

            ],401);

        }


    }

    // Return nutrition list
    public function nutrition(Request $request){

        try {

            $nutrition = Nutrition::all();
            return $nutrition;

        } catch (Exception $e) {

            return response()->json([

                'error' => 'no persmission',
                'message' => 'no items found',
                'status' => 0,
                'code' => 401

            ],401);
        }


    }

    // Return nutrition cataories
    public function nutrition_catagories(Request $request){

        try {

            $nutrition = Nutritioncat::all();
            return $nutrition;

        } catch (Exception $e) {

            return response()->json([

                'error' => 'no persmission',
                'message' => 'no catagories found',
                'status' => 0,
                'code' => 401

            ],401);
        }


    }

    // Return blog
    public function blog(Request $request){

        try {

            $blog = Blog::all();
            return $blog;

        } catch (Exception $e) {

            return response()->json([

                'error' => 'no persmission',
                'message' => 'no articles found',
                'status' => 0,
                'code' => 401

            ],401);
        }


    }

    // Return store catagories
    public function store_catagories(Request $request){

        try {

            $storecat = Storecat::all();
            return $storecat;

        } catch (Exception $e) {

            return response()->json([

                'error' => 'no persmission',
                'message' => 'no catagories found',
                'status' => 0,
                'code' => 401

            ],401);
        }


    }

    // Return store items
    public function store(Request $request){

        try {

            $store = Store::all();
            return $store;


        } catch (Exception $e) {

            return response()->json([

                'error' => 'no persmission',
                'message' => 'no items found',
                'status' => 0,
                'code' => 401

            ],401);
        }


    }

    // Update or create profile
    public function profileUpdate(Request $request){

        $request->validate([
            'user_id' => 'required|string',
            'dob' => 'required|string',
            'height' => 'required|string',
            'weight' => 'required|string',
            'age' => 'required|string',
            'sex' => 'required|string',
            'ethnic' => 'required|string'
        ]);

        // Check if user exists
        if(Profile::where('user_id', $request->user_id)->exists()){

            // If user exists update profile
            Profile::where('user_id', $request->user_id)->update([

                'dob' => $request->dob,
                'height' => $request->height,
                'weight' => $request->weight,
                'age' => $request->age,
                'sex' => $request->sex,
                'ethnic' => $request->ethnic

            ]);

            return response()->json([
                'message' => 'profile updated',
                'response' => 'true',
                'code' => 200
            ], 200);

        }else{

            // If user does not exist create new profile
            Profile::create($request->all());

            return response()->json([
                'message' => 'profile created',
                'response' => 'true',
                'code' => 200
            ], 200);

        }



    }


    //Return weekly workouts
    public function monday(Request $request){

        $weekly = User::find($request->user()->id);
        return $weekly->week->monday;

    }

    public function tuesday(Request $request){

        $weekly = User::find($request->user()->id);
        return $weekly->week->tuesday;

    }

    public function wednesday(Request $request){

        $weekly = User::find($request->user()->id);
        return $weekly->week->wednesday;

    }
    public function thursday(Request $request){

        $weekly = User::find($request->user()->id);
        return $weekly->week->thursday;

    }
    public function friday(Request $request){

        $weekly = User::find($request->user()->id);
        return $weekly->week->friday;

    }
    public function saturday(Request $request){

        $weekly = User::find($request->user()->id);
        return $weekly->week->saturday;

    }
    public function sunday(Request $request){

        $weekly = User::find($request->user()->id);
        return $weekly->week->sunday;

    }
    public function getWeekly(Request $request){

        $weekly = User::find($request->user()->id);
        return $weekly->week;

    }

    // Return exercise all id
    public function nutritionAllid($id){

        try {

            $exercise = Nutrition::all()->where('catagorie_id',$id);

            if($exercise->isEmpty()) {

                return response()->json([

                    'message' => 'no items found',
                    'status' => 0

                ]);

            }else{
                return response()->json($exercise);
            }

        } catch (Exception $e) {

            return response()->json([

                'error' => 'no persmission',
                'message' => 'no items found',
                'status' => 0,
                'code' => 401

            ],401);

        }


    }

    // Return exercise all id
    public function nutrition_id($id){

        try {

            $exercise = Nutrition::findOrFail($id);

            return response()->json($exercise);


        } catch (Exception $e) {

            return response()->json([

                'error' => 'no persmission',
                'message' => 'no items found',
                'status' => 0,
                'code' => 401

            ],401);

        }


    }

}
