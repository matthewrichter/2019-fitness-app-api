<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nutrition;
use App\Nutritioncat;
use Illuminate\Support\Facades\Storage;

class NutritionController extends Controller
{

    public function index()
    {
        $nutrition = Nutritioncat::all();
        $nutritions = Nutrition::all();
        $catagories = Nutritioncat::orderBy('id', 'asc')->get();
        $lastid_n = Nutritioncat::orderBy('id', 'desc')->first();
        $last_n = $lastid_n['id'];
        $lastid_n = Nutrition::orderBy('id', 'desc')->first();
        $last_n = $lastid_n['id'];

        return view('nutrition.index', compact('nutritions','catagories', 'nutrition', 'last_n','lastid_n','last_n','lastid_n'));

    }


    public function create()
    {
        return 'This is create';
    }


    public function store(Request $request, Nutrition $nutrition)
    {
        $request->validate([
            'catagorie_name' => 'required',
            'img'     =>  'required|image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        $storefile = Storage::disk('public') ->put('images',$request->file('img'));
        $name = basename($storefile);

        $getID = Nutritioncat::where('catagorie_name',$request->catagorie_name)->first();

        $nutrition->create([
            'catagorie_id' => $getID->id,
            'catagorie_name' => $request->catagorie_name,
            'img' => $name,
            'recipe' => $request->recipe,
            'description' => $request->description,
            'calories' => $request->calories,
            'carbs' => $request->carbs,
            'protein' => $request->protein,
            'fat' => $request->fat
        ]);

        return redirect()->back()
            ->with('added','true');
    }


    public function show($id)
    {
        return Nutrition::all()->where('id',$id);
    }


    public function edit($id)
    {



    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy(Nutrition $nutrition)
    {
        $nutrition->delete();

        return redirect()->back()->with('deleted','true');
    }
}
