<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exercise;
use App\Exercisecat;
use Illuminate\Support\Facades\Storage;

class ExerciseController extends Controller
{

    public function index()
    {
        $exercises = Exercise::all();
        $catagories = Exercisecat::orderBy('id', 'asc')->get();
        $lastid_w = Exercisecat::orderBy('id', 'desc')->first();
        $last_w = $lastid_w['id'];
        $lastid_e = Exercise::orderBy('id', 'desc')->first();
        $last_e = $lastid_w['id'];

        return view('workouts.index', compact('exercises','catagories','last_w','lastid_w','last_e','lastid_e'));

    }


    public function create()
    {
        //
    }


    public function store(Request $request, Exercise $exercise)
    {
        $request->validate([
            'catagorie_name' => 'required',
            'img'     =>  'required|image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        $storefile = Storage::disk('public') ->put('images',$request->file('img'));
        $name = basename($storefile);

        $getID = Exercisecat::where('catagorie_name',$request->catagorie_name)->first();

        $exercise->create([
            'catagorie_id' => $getID->id,
            'catagorie_name' => $request->catagorie_name,
            'img' => $name,
            'tips' => $request->tips,
            'description' => $request->description,
            'exercise' => $request->exercise
        ]);

        return redirect()->back()
            ->with('added','true');

    }


    public function show($id)
    {
        return Exercise::all()->where('id',$id);
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy(Exercise $exercise)
    {
        $exercise->delete();

        return redirect()->back()->with('deleted','true');
    }
}
