<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Nutritioncat;

class Nutrition extends Model
{
    protected $fillable = ['catagorie_name', 'catagorie_id', 'img', 'recipe', 'description', 'calories','protein','carbs','fat'];

    public function nutritioncat(){

        return $this->hasOne(Nutritioncat::class, 'catagorie_id');

    }

}
