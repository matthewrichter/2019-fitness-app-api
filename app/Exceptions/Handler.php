<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
  
    protected $dontReport = [
        //
    ];


    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];


    public function report(Exception $exception)
    {
        parent::report($exception);
    }


    public function render($request, Exception $exception)
    {
//        if($exception->getStatusCode()) {
//
//            if ($exception) {
//                // log the error
//                return response()->json([
//                    'code' => $exception->getStatusCode(),
//                    'message' => $exception->getMessage()
//                ]);
//            }
//
//        }else{
//
//
//        }


        

        return parent::render($request, $exception);
    }
}
