<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Nutrition;

class Nutritioncat extends Model
{

 protected $fillable = ['catagorie_name', 'catagorie_id','img'];

    public function nutrition(){

        return $this->hasOne(Nutrition::class, 'catagorie_id');

    }

}
