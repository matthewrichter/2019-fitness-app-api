<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = ['user_id','dob','height','weight','age','sex','ethnic'];

    public function user(){
        return $this->hasOne(User::class);
    }
}
