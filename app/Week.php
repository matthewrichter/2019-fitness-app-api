<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Week extends Model
{


    public function applicant()
    {
        return $this->hasMany('App\Applicant', 'user_id');
    }

    public function users() {
        return $this->hasMany('App\User', 'user_id');
    }

    public function bmis() {
        return $this->hasMany('App\Bmi', 'user_id');
    }}
