<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exercise extends Model
{

protected $fillable = ['catagorie_name', 'catagorie_id', 'img', 'tips', 'description', 'exercise'];

}
