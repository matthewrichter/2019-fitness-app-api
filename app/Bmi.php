<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bmi extends Model
{

    public function applicant()
    {
        return $this->hasOne('App\Applicant', 'user_id');
    }

    public function weeks() {
        return $this->hasMany('App\Week', 'user_id');
    }

    public function users() {
        return $this->hasMany('App\User', 'user_id');
    }


}
