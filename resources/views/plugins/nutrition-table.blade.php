<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" type="text/css">

<table id="profile-table" class="display">
    <thead class="thead-dark">
    <tr>
        <th scope="col">#</th>
        <th scope="col">Exercise Name</th>
        <th scope="col">Category Name</th>
        <th scope="col">Image</th>
        <th scope="col"></th>
        <th scope="col"></th>
    </tr>
    </thead>
    <tbody>

    @forelse($nutritions as $nutrition)

        <tr>
            <td>{{$nutrition->id}}</td>
            <td>{{$nutrition->recipe}}</td>
            <td>{{$nutrition->catagorie_name}}</td>
            <td><img src="images/{{$nutrition->img}}" height="50px"></td>
            <td><a href="{{ route('nutrition.show',$nutrition->id) }}"><button type="button" class="btn btn-dark btn-sm">Edit</button></a></td>
            <td>
                <form action="{{ route('nutrition.destroy',$nutrition->id) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-dark btn-sm" onclick="return confirm('Are you sure you want to remove this exercise?')">Remove</button>
                </form>
            </td>
        </tr>

    @empty

        <p>No users</p>

    @endforelse

    <!-- Nutrition Modal -->

    <div class="modal fade" id="addRecipe">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add a New Recipe</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('nutrition.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group container">
                                    <label>Recipe Name: </label> <input type="text" name="recipe" class="form-control" placeholder="e.g: Pushup" required><br>
                                    <select class="custom-select" name="catagorie_name">
                                        <option selected>Select a Recipe Category</option>
                                        @foreach($catagories as $category)
                                            <option value="{{$category->catagorie_name}}">{{$category->catagorie_name}}</option>
                                        @endforeach
                                    </select>

                                    <br><br>

                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Recipe Description</label>
                                        <textarea class="form-control" name="description" id="description" rows="3"></textarea>
                                    </div>

                                    <label>Calories: </label> <input type="text" name="calories" class="form-control" placeholder="e.g: 600" required>
                                    <label>Carbs: </label> <input type="text" name="carbs" class="form-control" placeholder="e.g: 45" required>
                                    <label>Protein: </label> <input type="text" name="protein" class="form-control" placeholder="e.g: 60" required>
                                    <label>Fat: </label> <input type="text" name="fat" class="form-control" placeholder="e.g: 18" required>
                                    <br>
                                    <input type="file" name="img" class="form-control"  name="Upload Image"/>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                <button type="submit" class="btn btn-primary" onclick="tinyMCE.triggerSave(true,true);">Add Recipe</button>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    </tbody>
</table>



<script src="//code.jquery.com/jquery-3.3.1.js"></script>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

<script>
    $('#profile-table').dataTable({

        columnDefs: [{
            orderable: false,
            className: 'select-checkbox',
            targets: 0
        }],
        select: {
            style: 'os',
            selector: 'td:first-child'
        }
    });
</script>

