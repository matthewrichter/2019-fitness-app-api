<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" type="text/css">


<table id="profile-table" class="display">
    <thead class="thead-dark">
    <tr>
        <th scope="col">#</th>
        <th scope="col">title</th>
        <th scope="col">img</th>
        <th scope="col"></th>
        <th scope="col"></th>
    </tr>
    </thead>
    <tbody>

    @forelse($blogs as $blog)

        <tr>
            <td>{{$blog->id}}</td>
            <td>{{$blog->title}}</td>
            <td><img src="images/{{$blog->img}}" height="50px"></td>
            <td><a href="{{ route('blogs.show',$blog->id) }}"><button type="button" class="btn btn-dark btn-sm">Edit</button></a></td>
            <td>
                <form action="{{ route('blogs.destroy',$blog->id) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-dark btn-sm" onclick="return confirm('Are you sure you want to remove this exercise?')">Remove</button>
                </form>
            </td>
        </tr>

    @empty

        <p>No blogs</p>

    @endforelse

    <!-- Blog Modal -->

    <div class="modal fade" id="addBlog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add a New Blog</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('blogs.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group container">
                                    <label>Blog Title: </label> <input type="text" name="title" class="form-control" placeholder="e.g: Pushup" required>
                                    <br><br>

                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Short Description</label>
                                        <textarea class="form-control" name="short" id="short" rows="2"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Full Article</label>
                                        <textarea class="form-control" name="description" id="description" rows="5"></textarea>
                                    </div>

                                    <input type="file" name="img" class="form-control"  name="Upload Image"/>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                <button type="submit" class="btn btn-primary" onclick="tinyMCE.triggerSave(true,true);">Add Recipe</button>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    </tbody>
</table>



<script src="//code.jquery.com/jquery-3.3.1.js"></script>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

<script>
    $('#profile-table').dataTable({

        columnDefs: [{
            orderable: false,
            className: 'select-checkbox',
            targets: 0
        }],
        select: {
            style: 'os',
            selector: 'td:first-child'
        }
    });
</script>

