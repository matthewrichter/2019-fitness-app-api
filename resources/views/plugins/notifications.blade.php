@if ($message = Session::get('deleted'))

    <body onload="notification()">

    <script>
        function notification() {

            $.notify("Category Deleted Successfully");

        }
    </script>
@endif

    @if ($message = Session::get('added'))

        <body onload="notification()">

        <script>
            function notification() {

                $.notify("Category Added Successfully");

            }
        </script>
@endif

        @if ($message = Session::get('updated1'))

            <body onload="notification()">

            <script>
                function notification() {

                    $.notify("Updated Successfully");

                }
            </script>
@endif
            @if ($message = Session::get('deletedb'))

                <body onload="notification()">

                <script>
                    function notification() {

                        $.notify("Blog Deleted Successfully");

                    }
                </script>
@endif

                @if ($message = Session::get('addedb'))

                    <body onload="notification()">

                    <script>
                        function notification() {

                            $.notify("Blog Added Successfully");

                        }
                    </script>
@endif