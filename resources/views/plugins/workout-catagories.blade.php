<table class="table table-striped table-bordered display compact" >
    <thead class="thead-dark">
    <tr>
        <th scope="col">Category ID</th>
        <th scope="col">Workout Categories</th>
        <th><button type="button" class="tn btn-outline-light btn-sm" data-toggle="modal" data-target="#addWorkout" data-whatever="@getbootstrap">Add</button></th>
       
    </tr>
    </thead>
    <tbody>

    @forelse($catagories as $catagorie)

        <tr>
            <td>{{$catagorie->catagorie_id}}</td>
            <td>{{$catagorie->catagorie_name}}</td>
            <td>
                <form action="{{ route('exercisecat.destroy',$catagorie->id) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-dark btn-sm" onclick="return confirm('Are you sure you want to remove this category?')">Remove</button>
                </form>
            </td>
             </tr>

    @empty

        <p>No Catagories</p>

    @endforelse
    
        <!-- Category Modal -->

    <div class="modal fade" id="addWorkout" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add a New Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('exercisecat.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group container">
                                    <input type="text" name="catagorie_name" class="form-control" placeholder="e.g: Arms" required>
                                    <input type="hidden" name="catagorie_id" value="{{$last_w + 1}}" class="form-control"><br>
                                    <input type="file" name="img" class="form-control"/>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">Add Category</button>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    </tbody>
</table>