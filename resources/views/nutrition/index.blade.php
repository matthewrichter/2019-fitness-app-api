@extends('layouts.app')

@section('content')

    @include('plugins.notifications')

    <div class="row">

        <div class="col-4">

            <div class="col-12">
                @include('plugins.nutrition-catagories')
            </div>

        </div>

        <div class="col-7">

            <div class="col-12">

                <table class="table table-striped table-bordered">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">Recipes</th>

                    </tr>
                    </thead>
                    <tbody>


                    </tbody>
                </table>
            </div>

            <div class="col-12">

                @include('plugins.nutrition-table')

            </div>
            <div class="text-right">
                <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#addRecipe" data-whatever="@getbootstrap">Add a New Exercise</button>
            </div><br>

        </div>
    </div>

    </div>


@endsection