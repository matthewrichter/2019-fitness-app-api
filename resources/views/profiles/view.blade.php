@extends('layouts.app')


@section('content')

    <style>
        .user-row {
            margin-bottom: 14px;
        }

        .user-row:last-child {
            margin-bottom: 0;
        }

        .table-user-information > tbody > tr {
            border-top: 1px solid rgb(221, 221, 221);
        }

        .table-user-information > tbody > tr:first-child {
            border-top: 0;
        }


        .table-user-information > tbody > tr > td {
            border-top: 0;
        }
        .toppad
        {margin-top:20px;
        }
    </style>

    @include('plugins.notifications')

    <div class="container">
        <div class="row">
            <div class="col-md-12  toppad  offset-md-0 ">
                <a href="{{ URL::previous() }}" class="btn btn-dark btn float-right">Go Back</a>
            </div>
            <div class="col-md-6  offset-md-0  toppad" >
                <div class="card">
                    <div class="card-body">
                        <h3 class="card-title">User Information</h3>
                        <form action="{{ route('view-profile.update',$user->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                        <table class="table table-user-information ">
                            <tbody>
                            <tr>
                                <td>Name:</td>
                                <td><input class="form-control" type="text" name="name" value="{{$user->name}}"></td>
                            </tr>
                            <tr>
                                <td>Email:</td>
                                <td><input class="form-control" type="text" name="email" value="{{$user->email}}"></td>
                            </tr>
                            <tr>
                                <td>Membership Started:</td>
                                <td><input class="form-control" type="text"  value="{{$user->created_at}}" disabled></td>
                            </tr>
                            <tr>
                                <td>Last Update:</td>
                                <td><input class="form-control" type="text"  value="{{$user->updated_at}}" disabled></td>
                            </tr>
                            </tbody>
                        </table>
                            <button type="submit" class="btn btn-dark btn" onclick="return confirm('Are you sure you want to save these changes?')">Save Changes</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6  offset-md-0  toppad" >
                <div class="card">
                    <div class="card-body">
                        <h3 class="card-title">Profile Information</h3>
                        <form action="{{ route('profile.update',$profile->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                        <table class="table table-user-information ">
                            <tbody>
                            <tr>
                                <td>Date Of Birth:</td>
                                <td><input class="form-control" type="text" name="dob" value="{{$profile->dob}}"></td>
                            </tr>
                            <tr>
                                <td>Height:</td>
                                <td><input class="form-control" type="text" name="height" value="{{$profile->height}}"></td>
                            </tr>
                            <tr>
                                <td>Weight:</td>
                                <td><input class="form-control" type="text" name="weight" value="{{$profile->weight}}"></td>
                            </tr>
                            <tr>
                                <td>Age:</td>
                                <td><input class="form-control" type="text" name="age" value="{{$profile->age}}"></td>
                            </tr>
                            <tr>
                                <td>Sex:</td>
                                <td><input class="form-control" type="text" name="sex" value="{{$profile->sex}}"></td>
                            </tr>
                            <tr>
                                <td>Ethnic:</td>
                                <td><input class="form-control" type="text" name="ethnic" value="{{$profile->ethnic}}"></td>
                            </tr>
                            </tbody>
                        </table>
                        <button type="submit" class="btn btn-dark btn" onclick="return confirm('Are you sure you want to save these changes?')">Save Changes</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection