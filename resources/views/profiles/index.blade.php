@extends('layouts.app')


@section('content')

    @include('plugins.notifications')
    
        <div class="row">

        <div class="container">
                <div class="col-12">

                    <table class="table table-striped table-bordered">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Profiles</th>

                        </thead>
                        <tbody>


                        </tbody>
                    </table>
                </div>

                <div class="col-12">

                @include('plugins.profile-table')

                </div>
        </div>
            </div>


@endsection