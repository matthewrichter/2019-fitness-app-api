@extends('layouts.app')

@section('content')

    @include('plugins.notifications')

    <div class="container">

    <div class="col-12">
        @include('plugins.workout-catagories')
    </div>

    <div class="col-12">
        @include('plugins.nutrition-catagories')
    </div>

    <div class="col-12">
        @include('plugins.store-catagories')
    </div>

    </div>

@endsection
