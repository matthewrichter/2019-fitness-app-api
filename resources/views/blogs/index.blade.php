@extends('layouts.app')


@section('content')

    @include('plugins.notifications')

    <div class="row">

        <div class="container">
            <div class="col-12">

                <table class="table table-striped table-bordered">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">Blogs</th>

                    </thead>
                    <tbody>


                    </tbody>
                </table>
            </div>

            <div class="col-12">

                @include('plugins.blog-table')

            </div>

            <div class="text-right">
                <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#addBlog" data-whatever="@getbootstrap">Add a New Blog</button>
            </div><br>
        </div>
    </div>


@endsection