<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('catagorie_id');
            $table->string('catagorie_name');
            $table->timestamps();
            $table->string('sku');
            $table->string('title');
            $table->longText('description');
            $table->integer('price');
            $table->longText('options');
            $table->string('m_img');
            $table->longText('img');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
