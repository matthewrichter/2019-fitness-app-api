<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBmisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bmis', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('user_id');
            $table->string('height');
            $table->string('weight');
            $table->string('age');
            $table->string('sex');
            $table->string('ethnic');
            $table->string('level');
            $table->string('bmi');
            $table->string('calories');
            $table->string('protein');
            $table->string('carbs');
            $table->string('fat');
            $table->string('goal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bmis');
    }
}
